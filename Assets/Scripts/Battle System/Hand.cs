﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour {
	List<Card> cardsInHand;

	public Deck drawPile;
	public Deck discardPile;

	void Awake(){
		cardsInHand = new List<Card> ();
	}

	public void Execute(Card card){
		card.Execute ();
	}

	public void Draw(){
		Card card = drawPile.DrawCard ();

		Card _card = Instantiate (card.gameObject, Vector3.zero, Quaternion.identity, transform).GetComponent<Card>();
		_card.OnCardDraw ();

		cardsInHand.Add (_card);
	}

	void Dispose(){

	}

	void Discard(){

	}
}
