﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Global;
using UnityEngine.UI;

public class Card : MonoBehaviour {
	[Header("Game Objects Config")]
	public Text costText;
	public Text descriptionText;
	public Text nameText;
	public Image background;
	public Image icon;

	[Header("Card Config")]
	public int cost;
	public string _name;
	public string description;

	public event VoidCallback onCardDraw;
	public event VoidCallback onCardDiscard;
	public event VoidCallback onCardDisposed;

	Effect[] effects;

	public void Execute(){
		foreach (var effect in effects) {
			effect.Execute ();
		}
	}

	protected void Awake(){
		effects = GetComponents<Effect> ();

		FormatCard ();
	}

	public void FormatCard(){
		costText.text = cost.ToString ();

		// It should be nice to get params on the text, and do some parsing or something on the text like "<attack>" and get the amount of attack
		descriptionText.text = description;
		nameText.text = _name;
	}

	public void OnCardDraw(){
		if (onCardDraw != null) {
			onCardDraw ();
		}
	}

	public void OnCardDiscard(){
		if (onCardDiscard != null) {
			onCardDiscard ();
		}
	}

	public void OnCardDisposed(){
		if (onCardDisposed != null) {
			onCardDisposed ();
		}
	}
}
