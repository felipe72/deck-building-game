﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Deck : MonoBehaviour {
	public List<Card> cards;

	Image cardBack;

	void Awake(){
		cardBack = GetComponent<Image> ();

		CheckEmpty ();
	}

	void CheckEmpty(){
		if (cards.Count == 0) {
			cardBack.color = Color.clear;
		} else {
			cardBack.color = Color.white;
		}
	}

	public Card DrawCard(){
		Card card = cards [0];
		RemoveCard (card);

		return card;
	}

	public bool IsCardInDeck(Card card){
		return true;
	}

	public void AddCard(Card card){
		CheckEmpty ();
	}

	public void RemoveCard(Card card){
		if (IsCardInDeck (card)) {
			cards.Remove (card);
		}

		CheckEmpty ();
	}

	public void RemoveCard(Card[] _cards){
		foreach (var card in _cards) {
			RemoveCard (card);
		}
	}

	public void AddCard(Card[] _cards){
		foreach (var card in _cards) {
			AddCard (card);
		}
	}

	public void Shuffle(){

	}

	public void ShuffleToDeck(Deck deck){
		deck.AddCard (cards.ToArray());
		RemoveCard (cards.ToArray());
	}
}
