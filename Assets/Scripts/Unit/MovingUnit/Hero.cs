﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Global;

public class Hero : MovingUnit {
	public int AP;
	public Vector2Int startingPos;

	void Start(){
		Initialize (Map.Instance.GetTileAt (startingPos.x, startingPos.y));
	}

	public List<Vector2Int> GetPath(Tile destinationTile){
		var list = Functions.ShortestPath (Map.Instance.GetMap, currentTile.Pos, destinationTile.Pos);

		list.Reverse ();

		if (list.Count > AP) {
			int diff = list.Count - AP;

			list.RemoveRange (list.Count - diff, diff); 
		}

		return list;
	}

	public List<Vector2Int> MovableTiles(){
		var list = Functions.FloodFill (Map.Instance.GetMap, currentTile.Pos, AP);

		return list;
	}

	public void Select(){
		Map.Instance.SelectHero (this);
		foreach (var pos in MovableTiles()) {
			Tile tile = Map.Instance.GetTileAt (pos.x, pos.y);

			tile.ChangeState (Tile.State.Movable);
		}
	}

	public void OnMouseDown(){
		Select ();
	}

	public void CleanMovable(){
		foreach (var x in MovableTiles()) {
			Tile tile = Map.Instance.GetTileAt (x.x, x.y);
			tile.ChangeState (Tile.State.Normal);
		}
	}

	public void CleanPath(Tile destinationTile){
		foreach (var x in GetPath(destinationTile)) {
			Tile tile = Map.Instance.GetTileAt (x.x, x.y);
			tile.ReturnState ();
		}
	}
}
