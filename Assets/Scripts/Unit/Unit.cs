﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {
	protected Tile currentTile;

	public void Initialize(Tile tile){
		currentTile = tile;
		currentTile.AddUnit (this);
		transform.position = new Vector3 (tile.transform.position.x, tile.transform.position.y + Map.Instance.tileHeight/2f);
	}

	public void OnMouseEnter(){
		if (currentTile != null) {
			currentTile.OnMouseEnter ();
		}
	}

	public void OnMouseExit(){
		if (currentTile != null) {
			currentTile.OnMouseExit ();
		}
	}
}
