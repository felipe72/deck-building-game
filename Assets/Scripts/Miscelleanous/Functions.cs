﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Global{
	public delegate void VoidCallback();

	public class Functions{
		public static T Randomize<T>(T[] array){
			int index = Random.Range (0, array.Length);
			return array [index];
		}

		public static T Randomize<T>(T[] array, ref int index){
			index = Random.Range (0, array.Length);
			return array [index];
		}

		public static void PrintMatrix<T>(T[,] matrix){
			string s = "";
			for (int i = 0; i < matrix.GetLength(0); i++) {
				for (int j = 0; j < matrix.GetLength(1); j++) {
					s += matrix [i, j].ToString () + " "; 
				}
				s += "\n";
			}

			Debug.Log (s);
		}

		public static List<Vector2Int> ShortestPath(Tile[,] matrix, Vector2Int start, Vector2Int end){
			int lenX = matrix.GetLength (0);
			int lenY = matrix.GetLength (1);

			Vector2Int[,] parents = new Vector2Int[lenX, lenY];
			bool[,] validMatrix = new bool[lenX, lenY];

			for (int x = 0; x < lenX; x++) {
				for (int y = 0; y < lenY; y++) {
					validMatrix [x, y] = matrix [x, y].isValid ();
					parents [x, y] = Vector2Int.one * -1;
				}
			}
			validMatrix [start.x, start.y] = true;
			validMatrix [end.x, end.y] = true;

			Vector2Int currPos = start;
			Queue<Vector2Int> queue = new Queue<Vector2Int>();
			Vector2Int[] dirs = new Vector2Int[]{ Vector2Int.up, Vector2Int.right, Vector2Int.down, Vector2Int.left };

			while (currPos != end) {
				if(!isValid(validMatrix, currPos.x, currPos.y)){
					continue;
				}

				foreach (var dir in dirs) {
					var pos = currPos + dir;
					if (isValid(validMatrix, pos.x, pos.y) && parents [pos.x, pos.y] == Vector2Int.one * -1) {
						queue.Enqueue (pos);
						parents [pos.x, pos.y] = currPos;
					}
				}

				validMatrix [currPos.x, currPos.y] = false;

				if (queue.Count == 0) {
					return new List<Vector2Int> ();
				}
				currPos = queue.Dequeue ();
			}

			List<Vector2Int> path = new List<Vector2Int> ();

			while (currPos != start) {
				path.Add (currPos);
				currPos = parents [currPos.x, currPos.y];
			}

			return path;
		}

		public static List<Vector2Int> FloodFill(Tile[,] matrix, Vector2Int start, int maxDistance){
			maxDistance += 1;

			int lenX = matrix.GetLength (0);
			int lenY = matrix.GetLength (1);

			bool[,] validMatrix = new bool[lenX, lenY];

			for (int x = 0; x < lenX; x++) {
				for (int y = 0; y < lenY; y++) {
					validMatrix [x, y] = matrix [x, y].isValid ();
				}
			}
			validMatrix [start.x, start.y] = true;

			Vector3Int currPos = new Vector3Int(start.x, start.y, maxDistance);
			Queue<Vector3Int> queue = new Queue<Vector3Int>();
			Vector3Int[] dirs = new Vector3Int[]{ Vector3Int.up, Vector3Int.right, Vector3Int.down, Vector3Int.left };

			for (int i = 0; i<dirs.Length; i++) {
				dirs[i].z = -1;
			}

			List<Vector2Int> flooded = new List<Vector2Int> ();
			queue.Enqueue (currPos);
	
			while (queue.Count != 0) {
				if(!isValid(validMatrix, currPos.x, currPos.y)  || currPos.z <= 0){
					currPos = queue.Dequeue ();
					continue;
				}

				flooded.Add (new Vector2Int(currPos.x, currPos.y));
				validMatrix [currPos.x, currPos.y] = false;

				foreach (var dir in dirs) {
					var pos = currPos + dir;
					queue.Enqueue (pos);
				}

				currPos = queue.Dequeue ();
			}

			return flooded;
		}


		static bool isValid(bool[,] matrix, int x, int y){
			int lenX = matrix.GetLength (0);
			int lenY = matrix.GetLength (1);

			return x >= 0 && x < lenX && y >= 0 && y < lenY && matrix[x, y];
		}
	}
}