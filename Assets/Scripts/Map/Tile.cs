﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Global;

public class Tile : MonoBehaviour {
	public event VoidCallback onTileEnter;
	public event VoidCallback onTileExit;

	public enum State { Normal, Highlighted, Movable };

	State currentState;
	State previousState;
	Color origColor;
	SpriteRenderer sr;
	Unit unit;

	Vector2Int pos;
	public Vector2Int Pos {
		get {
			return pos;
		}
	}

	void Awake(){
		sr = GetComponent<SpriteRenderer> ();
		origColor = sr.color;
	}

	public void Initialize(Vector2Int pos){
		this.pos = pos;
	}

	public void OnMouseEnter(){
		Map.Instance.HighlightTile (this);
	}

	public void ChangeState(State state){
		if (state == currentState) {
			return;
		}

		previousState = currentState;
		currentState = state;
		UpdateState ();
	}

	public void ReturnState(){
		currentState = previousState;
		UpdateState ();
	}

	public void UpdateState(){
		switch(currentState){
		case State.Highlighted:
			if (isValid ()) {
				sr.color = Color.green;
			} else {
				sr.color = Color.red;
			}
			break;
		case State.Movable:
			sr.color = Color.blue;
			break;
		case State.Normal:
			sr.color = origColor;
			break;
		}
	}

	public void OnMouseExit(){
		Map.Instance.DownlightTile (this);
		// ReturnState ();
	}

	public bool isValid(){
		return unit == null;
	}

	public void OnTileEnter(){
		if (onTileEnter != null) {
			onTileEnter ();
		}
	}

	public void OnTileExit(){
		if (onTileExit != null) {
			onTileExit ();
		}
	}

	public void AddUnit(Unit unit){
		this.unit = unit;
	}
}
