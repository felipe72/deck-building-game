﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Global;

public class Map : Singleton<Map> {
	[Header("Tile Configurations")]
	public GameObject tileTemplate;
	public Sprite[] baseTiles;
	public float tileWidth;
	public float tileHeight;

	[Header("Debug")]
	public GameObject mountain;

	[Header("Map Configurations")]
	public int height;
	public int width;

	Hero selectedHero;

	Tile[,] map;
	public Tile[,] GetMap {
		get {
			return map;
		}
	}

	Tile _highligtedTile;
	Tile highlightedTile{
		get {
			return _highligtedTile;
		} set {
			if (_highligtedTile != value) {
				_highligtedTile = value;
				if (_highligtedTile != null) {
					ShowPath ();
				}
			}
		}
	}

	void Awake(){
		map = new Tile[width,height];

		GenerateMap ();
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.J)) {
			Unit unit = Instantiate (mountain).GetComponent<Unit>();
			unit.Initialize (highlightedTile);
		}
	}

	void GenerateMap(){
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				CreateTileAt (x, y);
			}
		}

		float maxY = map[width-1, height-1].transform.position.y;

		Camera.main.transform.position = new Vector3 (0, maxY/2f, -10);
	}

	void CreateTileAt(int x, int y){
		float isoX = x - y;
		float isoY = (x + y)/2f;

		SpriteRenderer sr = Instantiate (tileTemplate, new Vector3 (isoX * tileWidth, isoY * tileHeight, (2*isoY)), Quaternion.identity, transform).GetComponent<SpriteRenderer>();
		sr.gameObject.name = string.Format ("{0} {1}", isoX, isoY);

		sr.sortingOrder = (int) -(2*isoY);
		sr.sprite = Functions.Randomize (baseTiles);

		Tile tile = sr.gameObject.GetComponent<Tile> ();
		tile.Initialize (new Vector2Int (x, y));
		map [x, y] = tile;
	}

	void ShowPath(){
		if (selectedHero == null || highlightedTile == null) {
			return;
		}

		var list = selectedHero.GetPath (highlightedTile);

		foreach (var x in list) {
			map [x.x, x.y].ChangeState (Tile.State.Highlighted);
		}
	}

	public void HighlightTile(Tile tile){
		highlightedTile = tile;
	}

	public void DownlightTile(Tile tile){
		if (selectedHero) {
			selectedHero.CleanPath (tile);
		}

		highlightedTile = null;

		UpdateMap ();
	}

	void UpdateMap(){
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				map [x, y].UpdateState ();
			}
		}
	}

	public Tile GetTileAt(int x, int y){
		return map [x, y];
	}

	public void SelectHero(Hero hero){
		if (selectedHero) {
			selectedHero.CleanMovable ();
		}

		selectedHero = hero;
		ShowPath ();
	}
}
